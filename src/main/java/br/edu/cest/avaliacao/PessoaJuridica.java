package br.edu.cest.avaliacao;

public class PessoaJuridica implements IPessoa{
	
	private String cnpj;
	private String endereco;
	private String nome;
	
	@Override
	public String getNome() {
		return this.nome;
	}
	
	@Override
	public String getEndereco() {
		return this.endereco;
	}


	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public PessoaJuridica(String cnpj, String nome) {
		this.cnpj = cnpj;
		this.nome = nome;
	}

}
