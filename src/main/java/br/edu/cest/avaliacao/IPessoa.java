package br.edu.cest.avaliacao;

public interface IPessoa {
	public String getEndereco();
	public String getNome();

}
