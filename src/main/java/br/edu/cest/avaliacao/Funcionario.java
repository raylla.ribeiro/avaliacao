package br.edu.cest.avaliacao;

import java.math.BigDecimal;

public class Funcionario extends PessoaFisica{
	
	private String cargo;
	private BigDecimal salario;
	
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public BigDecimal getSalario() {
		return salario;
	}
	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}
	
	public Funcionario(String nome, String cpf, String cargo, BigDecimal salario) {
		super(nome, cpf);
		this.cargo = cargo;
		this.salario = salario;
	}

}
