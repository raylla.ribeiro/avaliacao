package br.edu.cest.avaliacao;

public class PessoaFisica implements IPessoa{
	
	private String nome;
	private String cpf;
	private String endereco;
	
	@Override
	public String getNome() {
		return this.nome;
	}
	
	@Override
	public String getEndereco() {
		return this.endereco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public PessoaFisica(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}
	

}
